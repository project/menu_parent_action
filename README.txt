
Summary 

This module provides a way to place multiple nodes within a menu.  This module was designed to be used with Views Bulk Operations (http://drupal.org/project/views_bulk_operations).

The original use case for this module was to place nodes imported with the Node Import (http://drupal.org/project/node_import) module into the menu structure after import.

This module was originally developed for the Center for Research Libraries website, http://www.crl.edu.

Requirements

* Drupal 6

Installation

Install as usual, see http://drupal.org/node/70151 for further information.

Usage

The easiest way to learn how to use the module is to see it in action.  A screencast is available at http://screenr.com/ZXN (part 1) and http://screenr.com/FXN (part 2).

If a menu item already exists for the node, this action just updates the parent.  If a menu item does not exist, a new item with the specified parent is created with a title that is the same as the node's title.

Contact

Current maintainers:
* Geoffrey Hing (ghing) - http://drupal.org/user/47168
